import sys
from PyQt5 import QtWidgets, QtCore, QtGui
from scrape import get_recent_news


class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
    def __init__(self, icon, parent=None):
        QtWidgets.QSystemTrayIcon.__init__(self, icon, parent)

        self.menu = QtWidgets.QMenu(parent)
        self.menu.aboutToShow.connect(
            self.build_menu
        )  # Fetch every time menu is opened

        # Action for closing the program
        self.close_action = QtWidgets.QAction("Zatvori")
        self.close_action.triggered.connect(self.exit)

        self.setContextMenu(self.menu)

    def build_menu(self):
        news = get_recent_news()

        # Fetch last news
        self.menu.clear()

        for n in news:
            self.menu.addAction(n)

        self.menu.addAction(self.close_action)

    def exit(self):
        QtCore.QCoreApplication.exit()


def main():
    app = QtWidgets.QApplication(sys.argv)
    w = QtWidgets.QWidget()
    tray_icon = SystemTrayIcon(QtGui.QIcon("icon.png"), w)
    tray_icon.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
