# MFBL news scraper

Simple web scraping python script that reads news from faculty web site and displays them in tray/taskbar.

The screenshot shows the program in GNOME desktop environment.

![screenshot](screenshots/1.png "Screenshot")