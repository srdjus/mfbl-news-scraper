import requests
from bs4 import BeautifulSoup
import random


def get_recent_news():
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
        "Accept": "text/html",
    }

    response = requests.get("http://mf.unibl.org/oglasna-tabla", headers)

    soup = BeautifulSoup(response.text, features="html.parser")

    headings = soup.find_all("a", {"class": "vc_gitem-link"})

    # Just 10 
    return [heading["title"] for heading in headings[:10]]
